## LIS 3781 - Advanced Database Mangement 

## Elijah Butts

### Assignment 3 # Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Use Oracle SQL Developer Login
3. Create and Populate Oracle Tables
4. Query Result Sets, and SQL Code 
5. Same Number of Tables and Records
6. Bitbucket repo links:
    a)  this assignment
    b) the completed tutorial (bitbucketstationloactions)

### Assignment # 3 Business Rules

![App Screen Shot 1](img/busi.png)


#### README.md file Includes:

* Screenshot of A3 SQL Queries
* SQL Solutions / Output
* Screenshot of Populated Tables
* Tables from Select Statement

> #### Bitbucket Links:

LIS 4369: https://bitbucket.org/esb17b/lis33781/src/master/

BitbucketStationLocations: https://bitbucket.org/esb17b/bitbucketstationlocations/src/master/

#### Assignment Screenshots:

*Screenshot of Queries*:

![App Screen Shot 1](img/q1.png)

*Screenshot of SQL Query cont.*:

![App Screen Shot 2](img/q1-2.png)

*Screenshot of SQL Query cont.*:

![App Screen Shot 2](img/q1-3.png)

*Screenshot of SQL Query Output*:

> #### Tables:

![App Screen Shot 1](img/o1.png)

> #### Customer Table:

![App Screen Shot 2](img/o1-2.png)

> #### Commodity Table:

![App Screen Shot 2](img/o1-3.png)

> #### Order Table:

![App Screen Shot 2](img/o1-4.png)


