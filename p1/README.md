## LIS 3781 - Advanced Database Mangement 

## Elijah Butts

### Project 1 # Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. User Stored Procedures
3. Use Views/Triggers/Stored Procedures
4. Populate Tables 
5. Screenshots of Code
6. Bitbucket repo links:
    a)  this assignment
    b) the completed tutorial (bitbucketstationloactions)

### Project # 1 Business Rules

![App Screen Shot 1](img/busi.png)
![App Screen Shot 1](img/busi2.png)


#### README.md file Includes:

* Screenshot of P1 SQL Queries
* SQL Solutions / Output
* Screenshot of Populated Tables
* Tables from Select Statement

> #### Bitbucket Links:

LIS 4369: https://bitbucket.org/esb17b/lis33781/src/master/

BitbucketStationLocations: https://bitbucket.org/esb17b/bitbucketstationlocations/src/master/

#### Assignment Screenshots:

> #### ERD:
![App Screen Shot 1](img/erd.png)


*Screenshot of Queries*:

![App Screen Shot 1](img/q1.png)

*Screenshot of SQL Query cont.*:

![App Screen Shot 2](img/q1-2.png)

*Screenshot of SQL Query (Insert Statements).*:

![App Screen Shot 2](img/q1-3.png)

*Screenshot of SQL Query (SSN Hashing).*:

![App Screen Shot 2](img/q1-4.png)

*Screenshot of SQL Query Output*:

> #### List of Tables and Populated Table Ex.:

| Tables                           | Populated Table                    |
| -------------------------------- | ---------------------------------- |
| ![App Screen Shot 1](img/t1.png) | ![App Screen Shot 2](img/t1-2.png) |

> #### Creating a View:

![App Screen Shot 2](img/v1.png)

> #### View Output:

![App Screen Shot 2](img/v1-2.png)

> #### Creating a Trigger:

![App Screen Shot 2](img/tr.png)

> #### Trigger Output Before and After:

| Before Trigger                     | After Trigger                              |
| ---------------------------------- | ---------------------------------- |
| ![App Screen Shot 1](img/tr-2.png) | ![App Screen Shot 2](img/tr-3.png) |


