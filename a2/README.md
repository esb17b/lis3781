## LIS 3781 - Advanced Database Mangement 

## Elijah Butts

### Assignment 2 # Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket

2. Forward Install to CCI Server (not in workbench)

3. Indexes and Foreign Keys

4. Query Result Sets, and SQL Code 

5. Bitbucket repo links:
    a)  this assignment
    b) the completed tutorial (bitbucketstationloactions)

### Assignment # 2 Business Rules

![App Screen Shot 1](img/busi.png)


#### README.md file Includes:

* Screenshot of A2 SQL Queries
* SQL Solutions / Output
* Tables from select statement

> #### Bitbucket Links:

LIS 4369: https://bitbucket.org/esb17b/lis33781/src/master/

BitbucketStationLocations: https://bitbucket.org/esb17b/bitbucketstationlocations/src/master/

#### Assignment Screenshots:

*Screenshot of Queries*:

![App Screen Shot 1](img/q1.png)

*Screenshot of SQL Query cont.*:

![App Screen Shot 2](img/q1-2.png)

*Screenshot of SQL Query cont.*:

![App Screen Shot 2](img/q1-3.png)

*Screenshot of SQL Query Output*:

> #### Company:

![App Screen Shot 1](img/o1.png)

> #### Customer:

![App Screen Shot 2](img/o1-1.png)


