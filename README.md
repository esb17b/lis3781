# LIS 3781 Advanced Database Mangement

## Elijah Butts

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
   
    - Install AMPPS
    - Installation Screenshots
    - Install SQL Workbench
    - Create Bitbucket repo
    - Complete Bitbucket tutorial
    - Provide git command descriptions
    
2. [A2 README.md](a2/README.md "My A2 README.md file")
   
    - Distributed Version Control with Git and Bitbucket
    - Forward Install to CCI Server (not in workbench)
    - Indexes and Foreign Keys
    - Query Result Sets, and SQL Code
    - Database present on local machine 
   
3. [A3 README.md](a3/README.md "My A3 README.md file")
   
    - Use Oracle SQL Developer Login
    - Create and Populate Oracle Tables
    - Query Result Sets, and SQL Code
    - Same Number of Tables and Records
    - Connect to Virtual Machine 
    
4. [A4 README.md](a4/README.md "My A4 README.md file")
   
    - Use MS SQL Server
    - Create and Populate Tables Using MS SQL
    - Push Tables to Database
    - Abide by Business Rules
    - Connect to Virtual Machine
    - Query Result Sets, and SQL Code
    
    [A5 README.md](a5/README.md "My A5 README.md file")
    
    - TBA 

### Projects:

1. [P1 README.md](p1/README.md "My A3 README.md file")

    - Distributed Version Control with Git and Bitbucket 
    - User Stored Procedures
    - Use Views/Triggers/Stored Proccedures
    - Populate Tables 
    - Screenshots of Code

2. [P2 README.md](p2/README.md "My P2 README.md file")

    - Use MongoDB Server
    - Import JSON
    - Query Result Sets, and MongoDB methods
    - Connect to Atlas Server