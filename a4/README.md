## LIS 3781 - Advanced Database Mangement 

## Elijah Butts

### Assignment 4 # Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Use MS SQL Server
3. Create and Populate MS SQL Tables
4. Query Result Sets, and SQL Code 
5. Same Number of Tables and Records
6. Bitbucket repo links:
    a)  this assignment
    b) the completed tutorial (bitbucketstationloactions)

### Assignment # 4 Business Rules

![App Screen Shot 1](img/busi.png)
![App Screen Shot 1](img/busi2.png)
![App Screen Shot 1](img/busi3.png)

#### README.md file Includes:

* Screenshot of A4 SQL Queries
* SQL Solutions / Output
* Screenshot of Populated Tables
* Tables from Select Statement

> #### Bitbucket Links:

LIS 4369: https://bitbucket.org/esb17b/lis33781/src/master/

BitbucketStationLocations: https://bitbucket.org/esb17b/bitbucketstationlocations/src/master/

#### Assignment Screenshots:

> #### ERD:
![App Screen Shot 1](img/erd.png)

*Screenshot of Queries*:

![App Screen Shot 1](img/q1.png)

*Screenshot of SQL Query cont.*:

![App Screen Shot 2](img/q1-2.png)

*Screenshot of SQL Query cont.*:

![App Screen Shot 2](img/q1-3.png)

*Screenshot of SQL Query (Insert Statements).*:

![App Screen Shot 1](img/q1-4.png)

*Screenshot of SQL Query (Insert Statements) cont.*:

![App Screen Shot 2](img/q1-5.png)

*Screenshot of SQL Query (Insert Statements) cont.*:

![App Screen Shot 2](img/q1-6.png)

*Screenshot of SQL Query Output*:

> #### List of Tables and Populated Table Ex.:

| Populated Tables                           | Populated Tables                    |
| -------------------------------- | ---------------------------------- |
| ![App Screen Shot 1](img/t1.png) | ![App Screen Shot 2](img/t2.png) |

> #### More Tables:

![App Screen Shot 2](img/t3.png)

> #### More Tables:

![App Screen Shot 2](img/t4.png)

> #### Creating a Procedure:

![App Screen Shot 2](img/pr.png)

> #### Procedure Output:

| Sales Rep                     | Sale Rep History                              |
| ---------------------------------- | ---------------------------------- |
| ![App Screen Shot 1](img/pr-2.png) | ![App Screen Shot 2](img/pr-3.png) |