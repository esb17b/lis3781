## LIS 3781 - Advanced Database Mangement 

## Elijah Butts

### Assignment 1 # Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket

2. AMPPS Installation

3. Questions

4. Entity Relationship Diagram, and SQL Code (optional)

5. Bitbucket repo links:
    a)  this assignment / completed tutorial
    b) the completed tutorial (bitbucketstationloactions)

### Assignment # 1 Business Rules

The human resource (HR) department of the ACME company wants to contract a database
modeler/designer to collect the following employee data for tax purposes: job description, length of employment, benefits, number of dependents and their relationships, DOB of both the employee and any respective dependents. In addition, employees’ histories must be tracked. Also, include the following business rules:

```markdown
• Each employee may have one or more dependents. 
• Each employee has only one job. 
• Each job can be held by many employees. 
• Many employees may receive many benefits. 
• Many benefits may be selected by many employees (though, while they may not select any benefits—any dependents of employees may be on an employee’s plan). 
• Employee/Dependent tables must use suitable attributes (See Assignment Guidelines);
```

In Addition:

```markdown
• Employee: SSN, DOB, start/end dates, salary; 
• Dependent: same information as their associated employee (though, not start/end dates), date added (as dependent), type of relationship: e.g., father, mother, etc. 
• Job: title (e.g., secretary, service tech., manager, cashier, janitor, IT, etc.) 
• Benefit: name (e.g., medical, dental, long-term disability, 401k, term life insurance, etc.) 
• Plan: type (single, spouse, family), cost, election date (plans mustbe unique) 
• Employee history: jobs, salaries, and benefit changes, as well as who made the change and why; 
• Zero Filled data: SSN, zip codes (not phone numbers: US area codes not below 201, NJ); 
• All tables must include notes attribute.
```

#### README.md file Includes:

* Screenshot of A1 ERD
* SQL Solutions 
* Git commands with description

> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git-config - Get and set repository or global options

> #### Bitbucket Links:

LIS 4369: https://bitbucket.org/esb17b/lis33781/src/master/

BitbucketStationLocations: https://bitbucket.org/esb17b/bitbucketstationlocations/src/master/

#### Assignment Screenshots:

*Screenshot of Install*:

| Filezilla                          | AMPPS                              |
| ---------------------------------- | ---------------------------------- |
| ![App Screen Shot 1](img/zilla.png) | ![App Screen Shot 2](img/ampps.png) |

*Screenshot of ERD*:
![App Screen Shot 1](img/erd.png)

*Screenshot of SQL Query 1*:

![App Screen Shot 2](img/q1.png)![App Screen Shot 2](img/q1-1.png)

*Screenshot of SQL Query 2*:

![App Screen Shot 2](img/q2.png)![App Screen Shot 2](img/q2-1.png)

*Screenshot of SQL Query 3*:

![App Screen Shot 2](img/q3.png)![App Screen Shot 2](img/q3-1.png)

*Screenshot of SQL Query 4*:

![App Screen Shot 2](img/q4.png)

| Before                             | After                              |
| ---------------------------------- | ---------------------------------- |
| ![App Screen Shot 1](img/q4-1.png) | ![App Screen Shot 2](img/q4-2.png) |

*Screenshot of SQL Query 6*:

![App Screen Shot 2](img/q6.png)![App Screen Shot 2](img/q6-1.png)

