## LIS 3781 - Advanced Database Mangement 

## Elijah Butts

### Project 2 # Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Use MongoDB Server
3. Import JSON
4. Query Result Sets, and MongoDB methods
6. Bitbucket repo links:
    a)  this assignment
    b) the completed tutorial (bitbucketstationloactions)

### Project # 2 Business Rules

![App Screen Shot 1](img/business.png)

#### README.md file Includes:

* Screenshot of P2 Queries
* MongoDB Solutions / Output
* Screenshot Server Connection
* Screenshots of Statements

> #### Bitbucket Links:

LIS 4369: https://bitbucket.org/esb17b/lis33781/src/master/

BitbucketStationLocations: https://bitbucket.org/esb17b/bitbucketstationlocations/src/master/

#### Project Screenshots:

> #### Atlas Connection :
![App Screen Shot 1](img/atlas.png)

*Screenshot of Import Statement*:

![App Screen Shot 1](img/q1.png)

*show db / use / show collections.*:

![App Screen Shot 2](img/q1-2.png)

*Finding total number of restaraunts.*:

![App Screen Shot 2](img/q1-3.png)

*Example of one restaurant.*:

![App Screen Shot 1](img/q1-4.png)

*Number of Restaurants in the Brooklyn Bourough.*:

![App Screen Shot 2](img/q1-6.png)

*Restaurants in the Brooklyn Bourough.*:

![App Screen Shot 2](img/q1-5.png)

*pretty ( ) method.*:

![App Screen Shot 2](img/pretty.png)

> #### More Examples.:

| count() method                          | find() method                    |
| -------------------------------- | ---------------------------------- |
| ![App Screen Shot 1](img/t1.png) | ![App Screen Shot 2](img/t2.png) |

